package com.flink.uploader.service;


import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.flink.uploader.models.Query;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Properties;

@Service
public class FlinkClient {

    public FlinkClient() {
    }

    @Value("${flink.management.url}")
    private String flinkManagementUrl;

    @Value("${flink.class.name}")
    private String flinkClassName;

    public Object submitJob(Query query) throws IOException {
        CloseableHttpClient client = HttpClients.createDefault();
        HttpPost httpPost = new HttpPost(enrichRequestUrlwithParams(query.toMap()));
        //httpPost.setEntity(new StringEntity(generateRequestBody(), ContentType.APPLICATION_JSON));
        httpPost.setHeader("Accept", "application/json");
        httpPost.setHeader("Content-type", "application/json");
        return client.execute(httpPost);
    }

    private String enrichRequestUrlwithParams(HashMap<String,String> params) throws JsonProcessingException, UnsupportedEncodingException {
        // TODO : If any variables needed to put in request params, jobArgs will be extended

        ObjectMapper mapper = new ObjectMapper();
        String jobArgs = mapper.writeValueAsString(params);
        return flinkManagementUrl + "?entry-class=" + flinkClassName+ "&programArg=" + URLEncoder.encode(jobArgs, StandardCharsets.UTF_8.toString());
    }
}
