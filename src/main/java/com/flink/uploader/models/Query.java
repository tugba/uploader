package com.flink.uploader.models;

import com.fasterxml.jackson.databind.ObjectMapper;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.util.HashMap;

@Entity
public class Query {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String name;
    private String topic;
    private String resultTopic;
    private String sqlQuery;

    public Query() {
    }

    public Query(String name, String topic, String resultTopic, String sqlQuery) {
        this.name = name;
        this.topic = topic;
        this.resultTopic = resultTopic;
        this.sqlQuery = sqlQuery;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTopic() {
        return topic;
    }

    public void setTopic(String topic) {
        this.topic = topic;
    }

    public String getResultTopic() {
        return resultTopic;
    }

    public void setResultTopic(String resultTopic) {
        this.resultTopic = resultTopic;
    }

    public String getSqlQuery() {
        return sqlQuery;
    }

    public void setSqlQuery(String sqlQuery) {
        this.sqlQuery = sqlQuery;
    }

    public HashMap<String, String> toMap(){
        HashMap<String, String> object = new HashMap<>();
        object.put("name",this.getName());
        object.put("topic",this.getTopic());
        object.put("resultTopic",this.getResultTopic());
        object.put("sqlQuery",this.getSqlQuery());
        return object;
    }

}
