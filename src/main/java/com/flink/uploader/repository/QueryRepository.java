package com.flink.uploader.repository;

import com.flink.uploader.models.Query;
import org.springframework.data.repository.CrudRepository;

public interface QueryRepository extends CrudRepository<Query, Long> {


}
