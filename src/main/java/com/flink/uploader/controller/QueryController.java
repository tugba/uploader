package com.flink.uploader.controller;

import com.flink.uploader.models.Query;
import com.flink.uploader.service.FlinkClient;
import org.apache.http.HttpResponse;
import org.apache.http.client.ResponseHandler;
import org.apache.http.impl.client.BasicResponseHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;

@RestController
@RequestMapping("/api/flink")
public class QueryController {

    @Autowired
    private FlinkClient flinkClient;

    @PostMapping("/query")
    public Object query(@RequestBody Query query) throws IOException {
        System.out.println("HELL OOOO");
        HttpResponse response = (HttpResponse) flinkClient.submitJob(query);
        ResponseHandler<String> handler = new BasicResponseHandler();
        return handler.handleResponse(response);
    }
}
